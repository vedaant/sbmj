import React, { Component } from 'react';
import Async from '../../../helpers/asyncComponent';
import PageHeader from '../../../components/utility/pageHeader';
import Box from '../../../components/utility/box';
import LayoutWrapper from '../../../components/utility/layoutWrapper';
import ContentHolder from '../../../components/utility/contentHolder';
import IntlMessages from '../../../components/utility/intlMessages';
import Button, { ButtonGroup } from '../../../components/uielements/button';
import Select, { SelectOption } from '../../../components/uielements/select';
import { rtl } from '../../../config/withDirection';
import Radio, { RadioGroup } from '../../../components/uielements/radio';
import { Col, Row, Icon } from 'antd';
import Input, {
  InputSearch,
  InputGroup,
  Textarea,
} from '../../../components/uielements/input';
const Option = SelectOption;

const Editor = (props) => <Async load={import(/* webpackChunkName: "forms-editor" */ '../../../components/uielements/editor')} componentProps={props} />;
const gutter = 16;
function uploadCallback(file) {
  return new Promise(
    (resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open('POST', 'https://api.imgur.com/3/image');
      xhr.setRequestHeader('Authorization', 'Client-ID 8d26ccd12712fca');
      const data = new FormData();
      data.append('image', file);
      xhr.send(data);
      xhr.addEventListener('load', () => {
        const response = JSON.parse(xhr.responseText);
        resolve(response);
      });
      xhr.addEventListener('error', () => {
        const error = JSON.parse(xhr.responseText);
        reject(error);
      });
    }
  );
}

export default class AntdTreeSelect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editorState: null,
      loading: false,
      iconLoading: false,
    };
  }
  
  handleOnChange = checkedValues => {};
  onChange4 = e => {
 this.setState({
   
   value4: e.target.value
 });
}; 
  render() {
    const onEditorStateChange = (editorState) => {
      this.setState({ editorState });
    }
    const editorOption = {
      style: { width: '90%', height: '70%' },
      editorState: this.state.editorState,
      toolbarClassName: 'home-toolbar',
      wrapperClassName: 'home-wrapper',
      editorClassName: 'home-editor',
      onEditorStateChange: onEditorStateChange,
      uploadCallback: uploadCallback,
      toolbar: { image: { uploadCallback: uploadCallback } },
    };

    return (<LayoutWrapper>
     
        <Box
          title="Kundali Information Form" >
          <ContentHolder>
            
            <InputGroup size="large" style={{ marginBottom: '15px' }}>
         
              <Col span="12">
                <Input  placeholder="Gotra"  />
              </Col>
              <Col span="12">
                <Input placeholder="Maternal Gotra" />
              </Col>
            </InputGroup>
              <InputGroup size="large" style={{ marginBottom: '15px' }}>
             
              <Col span="12">
                <Input placeholder="Kundaly Date  " />
              </Col>
              <Col span="12">
                <Input placeholder="Kundaly Place" />
              </Col>
            </InputGroup>
             
              <InputGroup size="large" style={{ marginBottom: '15px' }}>
              <Col span="12">
                <Input placeholder="Manglik" />
              </Col>
              <Col span="12">
                <Input placeholder="Moonsign" />
              </Col>
            </InputGroup>
           
             <InputGroup size="large" style={{ marginBottom: '15px' }}>
              <Col span="12">
                <Input placeholder="Tell us about your life " />
              </Col>
              <Col span="12">
                <Input placeholder="About You " />
              </Col>
            </InputGroup>
            
            
              
            <Button type="primary" style={{ marginleft:'314px '}}>
              {<IntlMessages id="Registration" />}
            </Button>

          </ContentHolder>
        </Box>
     
    </LayoutWrapper>);
  }
}

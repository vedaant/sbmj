import React, { Component } from 'react';
import { Col, Row, Icon } from 'antd';
import Input, {
  InputSearch,
  InputGroup,
  Textarea,
} from '../../../components/uielements/input';
import InputNumber from '../../../components/uielements/InputNumber';
import Select, { SelectOption } from '../../../components/uielements/select';
import DatePicker from '../../../components/uielements/datePicker';
import AutoComplete from '../../../components/uielements/autocomplete';
import PageHeader from '../../../components/utility/pageHeader';
import Box from '../../../components/utility/box';
import LayoutWrapper from '../../../components/utility/layoutWrapper';
import ContentHolder from '../../../components/utility/contentHolder';
import IntlMessages from '../../../components/utility/intlMessages';
import Button, { ButtonGroup } from '../../../components/uielements/button';

import { rtl } from '../../../config/withDirection';
import Radio, { RadioGroup } from '../../../components/uielements/radio';
const Option = SelectOption;

const selectBefore = (
  <Select defaultValue="Http://" style={{ width: 80 }}>
    <Option value="Http://">Http://</Option>
    <Option value="Https://">Https://</Option>
  </Select>
);
const selectAfter = (
  <Select defaultValue=".com" style={{ width: 70 }}>
    <Option value=".com">.com</Option>
    <Option value=".jp">.jp</Option>
    <Option value=".cn">.cn</Option>
    <Option value=".org">.org</Option>
  </Select>
);

export default class InputField extends Component {
  state = {
    dataSource: [],
  };
  handleChange = value => {
    this.setState({
      dataSource:
        !value || value.indexOf('@') >= 0
          ? []
          : [`${value}@gmail.com`, `${value}@163.com`, `${value}@qq.com`],
    });
  };


  handleOnChange = checkedValues => {};
  onChange4 = e => {
 this.setState({
   
   value4: e.target.value
 });
}; 

  render() {
    const margin = {
      margin: rtl === 'rtl' ? '0 0 8px 8px' : '0 8px 8px 0'
    };
    const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap',
    };
    const colStyle = {
      marginBottom: '16px',
    };
    const gutter = 16;
    return (
      <LayoutWrapper>
        {/* <PageHeader>
          <IntlMessages id="forms.input.header" />
        </PageHeader> */}
      
          <Col md={12} sm={12} xs={24}>
            <Box
              title="Manage Matrimonial" >
              <ContentHolder>
                
                <InputGroup size="large" style={{ marginBottom: '15px' }}>
             
                  <Col span="12">
                    <Input  placeholder="User Name*"  />
                  </Col>
                  <Col span="12">
                    <Input placeholder="Password*" />
                  </Col>
                </InputGroup>
                  <InputGroup size="large" style={{ marginBottom: '15px' }}>
                 
                  <Col span="12">
                    <Input placeholder="First Name *" />
                  </Col>
                  <Col span="12">
                    <Input placeholder="Last Name *" />
                  </Col>
                </InputGroup>
                 
                  <InputGroup size="large" style={{ marginBottom: '15px' }}>
                  <Col span="12">
                    <Input placeholder="Fathers Name" />
                  </Col>
                  <Col span="12">
                    <Input placeholder="Fathers Name" />
                  </Col>
                </InputGroup>
                <RadioGroup onChange={this.onChange4} value={this.state.value4}>
                  <Radio value={11}>Male</Radio>
                  <Radio value={22}>Female</Radio>
                  
                </RadioGroup>
                 <InputGroup size="large" style={{ marginBottom: '15px' }}>
                  <Col span="12">
                    <Input placeholder="Date Of Birth " />
                  </Col>
                  <Col span="12">
                    <Input placeholder="Height  " />
                  </Col>
                </InputGroup>
                   <InputGroup size="large" style={{ marginBottom: '15px' }}>
                  <Col span="12">
                    <Input placeholder="Weight" />
                  </Col>
                 
                </InputGroup>
                
                  
                <Button type="primary" style={{ marginleft:'314px '}}>
                  {<IntlMessages id="Registration" />}
                </Button>

              </ContentHolder>
            </Box>
          </Col>
        
    
       
      </LayoutWrapper>
    );
  }
}

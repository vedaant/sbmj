import React, { Component } from 'react';
import { Row, Col } from 'antd';
import Radio, { RadioGroup } from '../../../components/uielements/radio';
import Select, { SelectOption } from '../../../components/uielements/select'; 
import PageHeader from '../../../components/utility/pageHeader';
import Box from '../../../components/utility/box';
import LayoutWrapper from '../../../components/utility/layoutWrapper';
import ContentHolder from '../../../components/utility/contentHolder';
import basicStyle from '../../../config/basicStyle';
import IntlMessages from '../../../components/utility/intlMessages';
import { rtl } from '../../../config/withDirection';
import Input, {
  InputSearch,
  InputGroup,
  Textarea,
} from '../../../components/uielements/input';
import Button, { ButtonGroup } from '../../../components/uielements/button';
const Option = SelectOption;
const plainOptions = ['Apple', 'Pear', 'Orange'];
const options = [
  { label: 'Apple', value: 'Apple' },
  { label: 'Pear', value: 'Pear' },
  { label: 'Orange', value: 'Orange' }
];
const optionsWithDisabled = [
  { label: 'Apple', value: 'Apple' },
  { label: 'Pear', value: 'Pear' },
  { label: 'Orange', value: 'Orange', disabled: false }
];

export default class IsomorphicRadiobox extends Component {
  state = {
    value: 1,
    value1: 'Apple',
    value2: 'Apple',
    value3: 'Apple',
    value4: 11
  };
  onChange = e => {
    this.setState({
      value: e.target.value
    });
  };
  onChange1 = e => {
    this.setState({
      value1: e.target.value
    });
  };
  onChange2 = e => {
    this.setState({
      value2: e.target.value
    });
  };
  onChange3 = e => {
    this.setState({
      value3: e.target.value
    });
  };
  onChange4 = e => {
    this.setState({
      value4: e.target.value
    });
  };
  render() {
    const radioStyle = {
      display: 'block',
      height: '30px',
      lineHeight: '30px'
    };
    const { rowStyle, colStyle, gutter } = basicStyle;
    return (
      <LayoutWrapper>
        
        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={12} sm={12} xs={24} style={colStyle}>
            <Box
              title="Career Information Form"
              
            >
              <ContentHolder>
              <InputGroup size="large" style={{ marginBottom: '15px' }}>
              <Col span="12">
                <Input  placeholder="Income"/>
              </Col>
              <Col span="12">
                <Input  placeholder="Education"  />
              </Col>
            </InputGroup>
              <InputGroup size="large" style={{ marginBottom: '15px' }}>
              <Col span="12">
                <Input placeholder="Working Sector" />
              </Col>
              <Col span="12">
                <Input placeholder="Occupation" />
              </Col>
            </InputGroup>
            <InputGroup size="large" style={{ marginBottom: '15px' }}>
              <Col span="12">
                <Input placeholder="Employment" />
              </Col>
              <Col span="12">
                <Input placeholder="Contact Information" />
              </Col>
            </InputGroup>
            <InputGroup size="large" style={{ marginBottom: '15px' }}>
              <Col span="12">
                <Input placeholder="Mobile No" />
              </Col>
              <Col span="12">
                <Input placeholder="Landline No" />
              </Col>
            </InputGroup>
            <InputGroup size="large" style={{ marginBottom: '15px' }}>
              <Col span="12">
                <Input placeholder="Address" />
              </Col>
              <Col span="12">
                <Input placeholder="City" />
              </Col>
            </InputGroup>
             
              <InputGroup size="large" style={{ marginBottom: '15px' }}>
               <Col span="12">
                <Select defaultValue="select State" style={{ width: '100%' }}>
                <Option value="Option1">tg</Option>
                <Option value="Option2">ap</Option>
              </Select>
              </Col>
              <Col span="12">
                <Select defaultValue="select Countery" style={{ width: '100%' }}>
                <Option value="Option1">india</Option>
                <Option value="Option2">Usa</Option>
              </Select>
              </Col>
              </InputGroup>
              <Button type="primary" style={{ marginleft:'314px '}}>
              {<IntlMessages id="Registration" />}
            </Button>

            
             
              
              </ContentHolder>
            </Box>
          </Col>
       
        </Row>
      </LayoutWrapper>
    );
  }
}

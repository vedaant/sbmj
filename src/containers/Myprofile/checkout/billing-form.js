import React from 'react';
import Select, { SelectOption } from '../../../components/uielements/select';
import Checkbox from '../../../components/uielements/checkbox';
import InputBox from './input-box';
import IntlMessages from '../../../components/utility/intlMessages';
import { BillingFormWrapper, InputBoxWrapper } from './checkout.style';
import Box from '../../../components/utility/box';
import Tabs, { TabPane } from '../../../components/uielements/tabs';
import { Col, Row, Icon } from 'antd';
import Button, { ButtonGroup } from '../../../components/uielements/button';
import Input, {
  InputSearch,
  InputGroup,
  Textarea,
} from '../../../components/uielements/input';
import { rtl } from '../../../config/withDirection';
import Radio, { RadioGroup } from '../../../components/uielements/radio';
const Option = SelectOption;

class BillingForm extends React.Component {
  
  state = {
    "value4":11
  }
  handleOnChange = checkedValues => {};
     onChange4 = e => {
    this.setState({
      
      value4: e.target.value
    });
  };  
  render() {
    const margin = {
      margin: rtl === 'rtl' ? '0 0 8px 8px' : '0 8px 8px 0'
    };
    return (

      <Box>
          <Tabs defaultActiveKey="1" onChange={callback}>
            <TabPane tab="PERSONAL DETAILS" key="1">
               <InputGroup size="large" style={{ marginBottom: '15px' }}>
                  <Col span="6">
                    <Input  placeholder="Username (Mobile#)"/>
                  </Col>
                  <Col span="6">
                    <Input  placeholder="Password"  />
                  </Col>
                </InputGroup>
                  <InputGroup size="large" style={{ marginBottom: '15px' }}>
                  <Col span="6">
                    <Input placeholder="FIRST NAME" />
                  </Col>
                  <Col span="6">
                    <Input placeholder="LAST NAME" />
                  </Col>
                </InputGroup>
                 
                  <InputGroup size="large" style={{ marginBottom: '15px' }}>
                  <Col span="6">
                    <Input placeholder="FATHER's FIRST NAME, LAST NAME" />
                  </Col>
                  <Col span="6">
                    <Input placeholder="HUSBAND's FIRST NAME, LAST NAME" />
                  </Col>
                </InputGroup>
                 <InputGroup size="large" style={{ marginBottom: '15px' }}>
                  <Col span="6">
                    <Input placeholder="HOUSE NO" />
                  </Col>
                  <Col span="6">
                    <Input placeholder="STREET NO" />
                  </Col>
                </InputGroup>
                   <InputGroup size="large" style={{ marginBottom: '15px' }}>
                  <Col span="6">
                    <Input placeholder="CITY" />
                  </Col>
                  <Col span="6">
                    <Input placeholder="DIST" />
                  </Col>
                </InputGroup>
                 
                   <InputGroup size="large" style={{ marginBottom: '15px' }}>
                  <Col span="6">
                    <Input placeholder="PIN" />
                  </Col>
                  <Col span="18">
                    <Select defaultValue="Select Country" style={{ width: '33%' }}>
                    <Option value="Option1">India</Option>
                    <Option value="Option2">Usa</Option>
                  </Select>
                  </Col>
                </InputGroup>
                  <ButtonGroup style={margin}>
                  
                  <Button type="primary">
                    Go forward<Icon type="right" />
                  </Button>
                </ButtonGroup>
                 

                 
            </TabPane>
            <TabPane tab="CONTACT DETAILS" key="2">
             <InputGroup size="large" style={{ marginBottom: '15px' }}>
                  <Col span="6">
                    <Input  placeholder="LANDLINE"/>
                  </Col>
                  <Col span="6">
                    <Input  placeholder="MOBILE"  />
                  </Col>
                </InputGroup>
                  <InputGroup size="large" style={{ marginBottom: '15px' }}>
                  <Col span="6">
                    <Input placeholder="BLOOG GROUP" />
                  </Col>
                  <Col span="6">
                    <Input placeholder="QUALIFICATION" />
                  </Col>
                </InputGroup>
                 
                  <InputGroup size="large" style={{ marginBottom: '15px' }}>
                  <Col span="6">
                    <Input placeholder="PROFESSION" />
                  </Col>
                  <Col span="6">
                    <Input placeholder="DATE OF BIRTH" />
                  </Col>
                </InputGroup>

                <RadioGroup onChange={this.onChange4} value={this.state.value4}>
                  <Radio value={11}>Male</Radio>
                  <Radio value={22}>Female</Radio>
                  
                </RadioGroup>

                 <InputGroup size="large" style={{ marginBottom: '15px',marginTop:'9px' }}>
                  <Col span="6">
                    <Input placeholder="MARRIAGE DATE" />
                  </Col>
                  <Col span="6">
                    <Input placeholder="BIRTH PLACE" />
                  </Col>
                </InputGroup>
                   <InputGroup size="large" style={{ marginBottom: '15px' }}>
                  <Col span="6">
                    <Input placeholder="PLACE OF ORIGIN" />
                  </Col>
                  <Col span="6">
                    <Input placeholder="CONSTITUENCY" />
                  </Col>
                </InputGroup>
                 
                   <InputGroup size="large" style={{ marginBottom: '15px' }}>
                  <Col span="6">
                    <Input placeholder="GOTRA" />
                  </Col>
                     <Col span="6">
                    <Input placeholder="NAKH" />
                  </Col>
                
                </InputGroup>
                  <InputGroup size="large" style={{ marginBottom: '15px' }}>
                  <Col span="6">
                    <Input placeholder="KULDEVI NAME" />
                  </Col>
                     <Col span="6">
                    <Input placeholder="KULDEVI  PLACE" />
                  </Col>
                
                </InputGroup>
                  <ButtonGroup style={margin}>
                  <Button type="primary">
                    <Icon type="left" />Go back
                  </Button>
                  <Button type="primary">
                    Go forward<Icon type="right" />
                  </Button>
                </ButtonGroup>
            </TabPane>
            <TabPane tab="GOVERNAMENT INFORMATION" key="3">
              <InputGroup size="large" style={{ marginBottom: '15px' }}>
                  <Col span="6">
                    <Input  placeholder="AADHAR NO."/>
                  </Col>
                  <Col span="6">
                    <Input  placeholder="VOTER  ID NO."  />
                  </Col>
                </InputGroup>
                  <InputGroup size="large" style={{ marginBottom: '15px' }}>
                  <Col span="6">
                    <Input placeholder="PAN CARD NO." />
                  </Col>
                  <Col span="6">
                    <Input placeholder="RATION CARD" />
                  </Col>
                </InputGroup>
                 
                  <InputGroup size="large" style={{ marginBottom: '15px' }}>
                   <Col span="6">
                    <Select defaultValue="VRADDHA PENSION REQUIRED" style={{ width: '100%' }}>
                    <Option value="Option1">YES</Option>
                    <Option value="Option2">NO</Option>
                  </Select>
                  </Col>
                  <Col span="6">
                    <Select defaultValue="INSURANCE REQ" style={{ width: '100%' }}>
                    <Option value="Option1">YES</Option>
                    <Option value="Option2">NO</Option>
                  </Select>
                  </Col>
                  </InputGroup>
                 
                <h3>Bank Details</h3>
                 <InputGroup size="large" style={{ marginBottom: '15px' }}>
                   <Col span="18">
                    <Select defaultValue="Have BANK ACCOUNT" style={{ width: '33%' }}>
                    <Option value="Option1">YES</Option>
                    <Option value="Option2">NO</Option>
                  </Select>
                  </Col>
                  
                </InputGroup>
                   <InputGroup size="large" style={{ marginBottom: '15px' }}>
                   <Col span="6">
                    <Input placeholder="If Yes, BANK NAME" />
                  </Col>
                 
                </InputGroup>
                 
                   <InputGroup size="large" style={{ marginBottom: '15px' }}>
                   <Col span="18">
                    <Select defaultValue="TYPE OF MEMBERSHIP" style={{ width: '33%' }}>
                    <Option value="Option1">MEMBER</Option>
                    <Option value="Option2">LIFE MEMBER </Option>
                    <Option value="Option3"> SANRAKSHAK </Option>
                  </Select>
                  </Col>
                
                </InputGroup>
                
                  <ButtonGroup style={margin}>
                  <Button type="primary">
                    <Icon type="left" />Go back
                  </Button>
                 
                </ButtonGroup>

               <Button type="primary" style={{ marginleft:'314px '}}>
                  {<IntlMessages id="Registration" />}
                </Button>
            </TabPane>
          </Tabs>
        </Box>
 



    );
    function callback(key) {}
  }
   
}
export default BillingForm;
